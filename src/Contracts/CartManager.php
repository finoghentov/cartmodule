<?php
namespace RomarkCode\Cart\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;

interface CartManager
{
    /**
     * Setting user to Cart
     *
     * @param Authenticatable $user
     *
     * @return void
     */
    public function setUser(Authenticatable $user): void;
}
