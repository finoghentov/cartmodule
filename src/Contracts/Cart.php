<?php

namespace RomarkCode\Cart\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations;
use phpDocumentor\Reflection\Types\Integer;

interface Cart extends CartManager
{
    /**
     * Relation with user
     *
     * @return Relations\BelongsTo
     */
    public function user(): Relations\BelongsTo;

    /**
     * Relation with Cart's Items
     *
     * @return Relations\HasMany
     */
    public function items(): Relations\HasMany;

    /**
     * Return Cart's User
     *
     * @return Authenticatable
     */
    public function getUser(): ?Authenticatable;

    /**
     * Return all Cart's Items
     *
     * @return Collection
     */
    public function getItems(): Collection;

    /**
     * Return quantity of all items in Cart
     *
     * @return int
     */
    public function itemCount(): int;
}
