<?php
namespace RomarkCode\Cart\Providers;

use RomarkCode\Cart\CartManager;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;


class CartServiceProvider extends ServiceProvider
{
    public function register()
    {
        App::bind('cart', function (){
            return new CartManager();
        });
    }

    public function boot(){
        $this->loadMigrationsFrom(__DIR__.'../database/migrations');
    }
}
