<?php

namespace RomarkCode\Cart\Models;

use RomarkCode\Product\Contracts\Buyable;
use App\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use RomarkCode\Cart\Contracts\Cart as CartContract;
use Illuminate\Database\Eloquent\Relations;

class Cart extends Model implements CartContract
{

    protected $guarded = ['id'];

    /**
     * The Cart's user relationship
     *
     * @return Relations\BelongsTo
     */
    public function user() : Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The Cart's items
     *
     * @return Relations\HasMany
     */
    public function items() : Relations\HasMany
    {
        return $this->hasMany(CartItem::class, 'cart_id', 'id');
    }

    /**
     * Return Cart's User
     *
     * @return Authenticatable
     */
    public function getUser(): ?Authenticatable
    {
        return $this->user;
    }

    /**
     * Return all Cart's Items
     *
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * Return quantity of all items in Cart
     *
     * @return int
     */
    public function itemCount(): int
    {
        return $this->items->sum('qty');
    }

    /**
     * Setting user to Cart
     *
     * @param Authenticatable $user
     *
     * @return void
     */
    public function setUser(Authenticatable $user): void
    {
        $this->user_id = $user->id;
    }

    /**
     * Removing user from Cart
     *
     * @param Authenticatable $user
     *
     * @return void
     */
    public function removeUser(): void
    {
        $this->user_id = null;
    }

    /**
     * Setting user to Cart
     *
     * @return void
     */
    public function setSession(): void
    {
        $this->session_id = session()->getId();
    }

    /**
     * @inheritDoc
     */
    public function addItem(Buyable $product, $qty = 1, $params = []): \RomarkCode\Cart\Contracts\CartItem
    {
        $item = $this->items()->ofCart($this)->byProduct($product)->first();

        if ($item) {
            $item->qty += $qty;
            $item->save();
        } else {
            $item = $this->items()->create(
                array_merge(
                    $this->getDefaultCartItemAttributes($product, $qty),
                    $params['attributes'] ?? []
                )
            );
        }

        $this->load('items');

        return $item;
    }

    /**
     * Returns the default attributes of a Buyable for a cart item
     *
     * @param Buyable $product
     * @param int $qty
     *
     * @return array
     */
    protected function getDefaultCartItemAttributes(Buyable $product, $qty)
    {
        return [
            'product_id'   => $product->getId(),
            'qty'          => $qty,
            'price'        => $product->getPrice()
        ];
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->items()->ofCart($this)->delete();

        $this->load('items');
    }

    /**
     * @inheritDoc
     */
    public function removeItem($item)
    {
        if ($item) {
            $item->delete();
        }

        $this->load('items');
    }

    /**
     * @inheritDoc
     */
    public function removeProduct(Buyable $product)
    {
        $item = $this->items()->ofCart($this)->byProduct($product)->first();

        $this->removeItem($item);
    }

    /**
     * @inheritDoc
     */
    public function total()
    {
        return $this->items->sum('total');
    }
}
