<?php

namespace RomarkCode\Cart\Models;

use RomarkCode\Product\Contracts\Buyable;
use RomarkCode\Product\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use RomarkCode\Cart\Contracts\CartItem as CartItemContract;

/**
 * @property Product $product
 * @property float   $total
 */
class CartItem extends Model implements CartItemContract
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @inheritDoc
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity()
    {
        return $this->qty;
    }

    /**
     * @inheritDoc
     */
    public function total()
    {
        return $this->price * $this->qty;
    }

    /**
     * Property accessor alias to the total() method
     *
     * @return float
     */
    public function getTotalAttribute()
    {
        return $this->total();
    }

    /**
     * Scope to query items of a cart
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $cart Cart object or cart id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCart($query, $cart)
    {
        $cartId = is_object($cart) ? $cart->id : $cart;

        return $query->where('cart_id', $cartId);
    }

    /**
     * Scope to query items by product
     *
     * @param Builder $query
     * @param Buyable $product
     *
     * @return Builder
     */
    public function scopeByProduct($query, Buyable $product)
    {
        return $query->where([
            ['product_id', '=', $product->getId()],
        ]);
    }
}
