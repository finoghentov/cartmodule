<?php
/**
 * Contains the CartManager class.
 *
 * @copyright   Copyright (c) 2017 Attila Fulop
 * @author      Attila Fulop
 * @license     MIT
 * @since       2017-10-29
 *
 */

namespace RomarkCode\Cart;

use RomarkCode\Product\Contracts\Buyable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use RomarkCode\Cart\Contracts\Cart as CartContract;
use RomarkCode\Cart\Contracts\CartItem;
use RomarkCode\Cart\Contracts\CartManager as CartManagerContract;
use RomarkCode\Cart\Models\Cart;

class CartManager implements CartManagerContract
{
    protected $sessionKey;

    /** @var  Cart  The Cart model instance */
    protected $cart;

    public function __construct()
    {
        $this->sessionKey = 'cart_id';
    }

    /**
     * @inheritDoc
     */
    public function getItems(): Collection
    {
        return $this->exists() ? $this->model()->getItems() : new Collection();
    }

    /**
     * @inheritDoc
     */
    public function addItem(Buyable $product, $qty = 1, $params = []): CartItem
    {
        $cart = $this->findOrCreateCart();

        return $cart->addItem($product, $qty, $params);
    }

    /**
     * @inheritDoc
     */
    public function removeItem($item)
    {
        if ($cart = $this->model()) {
            $cart->removeItem($item);
        }
    }

    /**
     * @inheritDoc
     */
    public function removeProduct(Buyable $product)
    {
        if ($cart = $this->model()) {
            $cart->removeProduct($product);
        }
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        if ($cart = $this->model()) {
            $cart->clear();
        }
    }

    /**
     * @inheritDoc
     */
    public function itemCount(): int
    {
        return $this->exists() ? $this->model()->itemCount() : 0;
    }

    /**
     * @inheritDoc
     */
    public function total()
    {
        return $this->exists() ? $this->model()->total() : 0;
    }

    /**
     * @inheritDoc
     */
    public function exists()
    {
        return (bool) $this->getCartId() && $this->model();
    }

    /**
     * @inheritDoc
     */
    public function doesNotExist()
    {
        return !$this->exists();
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        $id = $this->getCartId();

        if ($id && $this->cart) {
            return $this->cart;
        } elseif ($id) {
            $this->cart = $this->findCart($id);
            return $this->cart;
        }

        return null;
    }

    private function findCart($id){
        if(!$cart = Cart::find($id)){
            $cart = Cart::where('session_id', session()->getId())->first();
        }

        return $cart;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty()
    {
        return 0 == $this->itemCount();
    }

    /**
     * @inheritDoc
     */
    public function isNotEmpty()
    {
        return !$this->isEmpty();
    }

    /**
     * @inheritDoc
     */
    public function destroy()
    {
        $this->clear();
        $this->model()->delete();
        $this->forget();
    }

    /**
     * @inheritdoc
     */
    public function create($forceCreateIfExists = false)
    {
        if ($this->exists() && !$forceCreateIfExists) {
            return;
        }

        $this->createCart();
    }

    /**
     * @inheritDoc
     */
    public function getUser()
    {
        return $this->exists() ? $this->model()->getUser() : null;
    }

    /**
     * @inheritDoc
     */
    public function setUser($user): void
    {
        if ($this->exists()) {
            $this->cart->setUser($user);
            $this->cart->setSession();
            $this->cart->save();
            $this->cart->load('user');
        }
    }

    /**
     * @inheritdoc
     */
    public function removeUser()
    {
        if ($this->exists()) {
            $this->cart->removeUser();
            $this->cart->save();
            $this->cart->load('user');
        }
    }

    /**
     * @inheritDoc
     */
    public function forget()
    {
        $this->cart = null;
        session()->forget($this->sessionKey);
    }

    /**
     * Returns the model id of the cart for the current session
     * or null if it does not exist
     *
     * @return int|null
     */
    protected function getCartId()
    {
        return session($this->sessionKey);
    }

    /**
     * Returns the cart model for the current session by either fetching it or creating one
     *
     * @return Cart
     */
    protected function findOrCreateCart()
    {
        return $this->model() ?: $this->createCart();
    }

    /**
     * Creates a new cart model and saves it's id in the session
     */
    protected function createCart()
    {
        if (Auth::check()) {
            $attributes = [
                'user_id' => Auth::user()->id
            ];
        }

        $attributes['session_id'] = session()->getId();

        return $this->setCartModel(Cart::create($attributes));
    }

    protected function setCartModel(CartContract $cart): CartContract
    {
        $this->cart = $cart;
        session()->put($this->sessionKey, $this->cart->id);

        return $this->cart;
    }
}
