<?php


namespace RomarkCode\Cart\Tests;


use RomarkCode\Cart\Facades\Cart;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class CartTest extends CartTesting
{
    /**
     * @test
     * @group Cart
     */
    public function get_cart_items(){
        Cart::addItem($this->first_product);
        Cart::addItem($this->second_product);
        $this->assertInstanceOf(Collection::class, Cart::getItems());
        $this->assertEquals(2, Cart::getItems()->count());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_is_not_empty(){
        Cart::addItem($this->first_product);
        Cart::addItem($this->second_product);
        $this->assertTrue(true, Cart::isNotEmpty());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_is_empty(){
        $this->assertTrue(true, Cart::isEmpty());
    }



    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_items_count(){
        $rand = rand(0, 10);$rand2 = rand(0, 10);
        Cart::addItem($this->first_product, $rand);
        Cart::addItem($this->second_product, $rand2);

        $this->assertEquals($rand + $rand2, Cart::itemCount());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_items_total_price(){
        $rand = rand(0, 10);
        Cart::addItem($this->first_product, $rand);
        Cart::addItem($this->second_product, $rand);
        $expectedPrice = $this->first_product->price * $rand + $this->second_product->price * $rand;

        $this->assertEquals($expectedPrice, Cart::total());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_forget(){
        Cart::addItem($this->first_product);
        Cart::forget();
        $this->assertEquals(null, Cart::model());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_destroy(){
        Cart::addItem($this->first_product);
        Cart::destroy();
        $this->assertEquals(true, Cart::doesNotExist());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_clear_cart(){
        Cart::addItem($this->first_product, 12);
        Cart::addItem($this->second_product, 10);
        Cart::clear();
        $this->assertEquals(true, Cart::isEmpty());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_exists(){
        Cart::addItem($this->first_product, 12);

        $this->assertEquals(true, Cart::exists());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_cart_not_exists(){
        $this->assertEquals(true, Cart::doesNotExist());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_check_creating_cart(){
        Cart::create();
        $this->assertEquals(true, Cart::exists());
    }

    /**
     * @test
     * @group Cart
     */
    public function test_getting_user_cart(){
        $user = User::first();
        Auth::login($user);
        Cart::create();
        $this->assertEquals($user->id, Cart::getUser()->id);
    }

    /**
     * @test
     * @group Cart
     */
    public function test_setting_user_to_cart(){
        $user = User::first();
        Cart::create();
        Cart::setUser($user);
        $this->assertEquals($user->id, Cart::getUser()->id);
    }

    /**
     * @test
     * @group Cart
     */
    public function test_removing_user_from_cart(){
        $user = User::first();
        Cart::create();
        Cart::setUser($user);
        Cart::removeUser();
        $this->assertEquals(null, Cart::getUser());
    }
}
