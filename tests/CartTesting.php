<?php


namespace RomarkCode\Cart\Tests;


use RomarkCode\Cart\Tests\Dummies\Product;
use Tests\TestCase;

abstract class CartTesting extends TestCase
{
    protected $first_product;
    protected $second_product;

    protected function setUp(): void
    {
        parent::setUp();
        $pathToFactories = __DIR__.'/Factories';
        $factory = \Illuminate\Database\Eloquent\Factory::construct(
            \Faker\Factory::create(),
            $pathToFactories
        );

        $this->first_product = $factory->of(Product::class)->create();
        $this->second_product = $factory->of(Product::class)->create();
    }
}
