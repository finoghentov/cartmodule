<?php
/**
 * Contains the Product class.
 *
 * @copyright   Copyright (c) 2017 Attila Fulop
 * @author      Attila Fulop
 * @license     MIT
 * @since       2017-10-29
 *
 */

namespace RomarkCode\Cart\Tests\Dummies;

use RomarkCode\Product\Traits\BuyableTrait;
use RomarkCode\Product\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements Buyable
{
    use BuyableTrait;

    protected $guarded = ['id'];
}
