<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use RomarkCode\Cart\Tests\Dummies\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $title = $faker->numerify('Product ###');

    return [
        'title' => $title,
        'slug' => $faker->unique()->ean8,
        'subtitle' => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'category_id' => $faker->numberBetween($min = 1, $max = 9),
        'qty' => $faker->randomDigit,
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 1000),
        'product_characteristics' =>json_encode([
            "MNV" => $faker->word(),
            "Manufacturer" => $faker->name($gender = null||'male'||'female'),
            "Size" => $faker->randomDigit .'Inches x'. $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 10),
            "Features" => $faker->paragraphs($nb = 3, $asText = false),
            "Dimensions" =>"Width" . $faker->randomDigit . '", Depth' . $faker->randomDigit . '", Height' . $faker->randomDigit . "Weight:" . $faker->randomDigit . "IBS"
        ]),
        'short_description' => $faker->sentence($nbWords = 10 , $variableNbWords = true),
        'description' => $faker->paragraph($nb = 7, $asText = false),
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'gallery' => json_encode([
            1 => $faker->imageUrl($width = 640, $height = 480),
            2 => $faker->imageUrl($width = 640, $height = 480),
            3 => $faker->imageUrl($width = 640, $height = 480),
        ]),
        'meta_title' => $faker->word,
        'meta_description' => $faker->paragraph($nb = 8, $asText = false),
        'order' => $faker->numberBetween($min = 0, $max = 1),
        'status' => $faker->numberBetween($min = 0, $max = 1),
        'in_stock' => $faker->numberBetween($min = 0, $max = 1),
    ];
});
