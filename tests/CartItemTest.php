<?php


namespace RomarkCode\Cart\Tests;


use RomarkCode\Cart\Facades\Cart;
use RomarkCode\Product\Contracts\Buyable;

class CartItemTest extends CartTesting
{
    /**
     * @test
     * @group CartItems
     */
    public function the_cart_item_return_buyable_type_and_same_id()
    {
        $this->withoutExceptionHandling();

        Cart::addItem($this->first_product);

        $product = Cart::model()->items->first()->product;
        $this->assertInstanceOf(Buyable::class, $product);
        $this->assertEquals($this->first_product->id, $product->id);
    }

    /**
     * @test
     * @group CartItems
     */
    public function test_add_item_to_cart()
    {
        Cart::addItem($this->first_product);

        $this->assertTrue(true, Cart::model()->items()->first());
    }

    /**
     * @test
     * @group CartItems
     */
    public function test_remove_product_from_cart(){
        Cart::addItem($this->first_product);
        Cart::addItem($this->second_product);
        Cart::removeProduct($this->second_product);
        $this->assertEquals($this->first_product->id, Cart::model()->items()->first()->product_id);
        $this->assertEquals(1, Cart::getItems()->count());
    }

    /**
     * @test
     * @group CartItems
     */
    public function test_remove_item_from_cart(){
        Cart::addItem($this->first_product);
        Cart::addItem($this->second_product);
        Cart::removeItem(Cart::model()->items()->first());
        $this->assertEquals(1, Cart::getItems()->count());
    }

    /**
     * @test
     * @group CartItems
     */
    public function test_total_price_item(){
        Cart::addItem($this->first_product, 5);
        $this->assertEquals($this->first_product->price * 5, Cart::model()->items()->first()->total());
    }
}
